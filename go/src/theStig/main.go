package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
)

const (
	debug bool = false
	//Test Track Names
	Finland string = "keimola"
	USA     string = "usa"
	Germany string = "germany"
	France  string = "france"
)

var (
	reader *GameMessageReader
	writer *BotActionWriter

	track          Track
	myCar          CarId
	cars           []*Car
	currentTick    int = 0
	prevPieceIndex int = 0
)

func Connect(host string, port int) (conn net.Conn, err error) {
	conn, err = net.Dial("tcp", fmt.Sprintf("%s:%d", host, port))
	return
}

func HandleMessage(msg Message) (err error) {
	currentTick = msg.GameTick
	switch msg.MsgType {
	case "join":
		log.Printf("Joined")
		writer.SendPing()
	case "yourCar":
		log.Printf("YourCar received")
		json.Unmarshal(msg.Data, &myCar)
	case "gameInit":
		log.Printf("GameInit received")
		SetRaceLogger(msg.GameId)
		HandleGameInitMessage(msg)
	case "gameStart":
		log.Printf(fmt.Sprintf("Game started: %s", msg.GameId))
		writer.SendPing()
	case "crash":
		log.Printf("Someone crashed")
		writer.SendPing()
	case "gameEnd":
		log.Printf("Game ended")
		writer.SendPing()
	case "carPositions":
		HandleCarPositionsMessage(msg)
	case "error":
		log.Printf(fmt.Sprintf("Got error: %v", msg.Data))
		writer.SendPing()
	default:
		log.Printf("Got msg type: %s", msg.MsgType)
		writer.SendPing()
	}
	return
}

func HandleGameInitMessage(msg Message) {
	var gameInit GameInit
	json.Unmarshal(msg.Data, &gameInit)
	track = gameInit.Race.Track
	cars = gameInit.Race.Cars
	racelog.Printf("Racing on track: %s", track.Name)
	CreateTrackPlan(track.Pieces)
}

func HandleCarPositionsMessage(msg Message) {
	var carPosistions []*CarPosition
	json.Unmarshal(msg.Data, &carPosistions)
	curPos := *FindMyCarPosition(carPosistions)
	piecePos := curPos.PiecePosition
	racelog.Printf(
		"GameTick: %d Angle: %f PieceIndex: %d InPieceDistance: %f",
		msg.GameTick, curPos.Angle, piecePos.PieceIndex,
		piecePos.InPieceDistance,
	)
	pieceIndex := piecePos.PieceIndex
	if prevPieceIndex != pieceIndex {
		dir := TrackPlan[pieceIndex]
		if dir != "" {
			writer.SendSwitchLane(dir)
		}
		prevPieceIndex = pieceIndex
	}
	writer.SendThrottle(CalculateThrottle(curPos))
	lastPosition = curPos
}

//TODO: This needs to eventually be re-worked to track all of the car positions
func FindMyCarPosition(carPositions []*CarPosition) (myPos *CarPosition) {
	for _, pos := range carPositions {
		if pos.Id == myCar {
			myPos = pos
			break
		}
	}
	return
}

func BotLoop(name string, key string) (err error) {
	writer.SendJoin(name, key)
	for {
		msg, err := reader.ReadMsg()
		if err != nil {
			return err
		}
		err = HandleMessage(msg)
		if err != nil {
			return err
		}
	}
	return
}

func ParseArgs() (host string, port int, name string, key string, err error) {
	args := os.Args[1:]
	if len(args) != 4 {
		return "", 0, "", "", errors.New("Usage: ./run host port botname botkey")
	}
	host = args[0]
	port, err = strconv.Atoi(args[1])
	if err != nil {
		return "", 0, "", "", errors.New(fmt.Sprintf("Could not parse port value to integer: %v\n", args[1]))
	}
	name = args[2]
	key = args[3]

	return
}

func main() {
	host, port, name, key, err := ParseArgs()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connecting with parameters:")
	fmt.Printf("host=%v, port=%v, bot name=%v, key=%v\n", host, port, name, key)

	conn, err := Connect(host, port)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	reader = CreateGameMessageReader(conn)
	writer = CreateBotActionWriter(conn)

	err = BotLoop(name, key)
	if err != nil {
		log.Fatal(err)
	}
}
