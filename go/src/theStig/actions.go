package main

import (
	"bufio"
	"encoding/json"
	"net"
)

type Direction string

const (
	Left  Direction = "Left"
	Right Direction = "Right"
)

type BotActionWriter struct {
	writer *bufio.Writer
}

func CreateBotActionWriter(conn net.Conn) *BotActionWriter {
	return &BotActionWriter{
		writer: bufio.NewWriter(conn),
	}
}

func (writer *BotActionWriter) SendJoin(name string, key string) (err error) {
	data := map[string]string{
		"name": name,
		"key":  key,
	}
	err = WriteMsg(writer.writer, "join", data)
	return
}

func (writer *BotActionWriter) SendJoinRace(name string, key string, carCount int, track string) (err error) {
	data := map[string]interface{}{
		"botId":     map[string]string{"name": name, "key": key},
		"trackName": track,
		"carCount":  carCount,
	}
	err = WriteMsg(writer.writer, "joinRace", data)
	return
}

func (writer *BotActionWriter) SendPing() (err error) {
	err = WriteMsg(writer.writer, "ping", nil)
	return
}

func (writer *BotActionWriter) SendThrottle(throttle float64) (err error) {
	err = WriteMsg(writer.writer, "throttle", throttle)
	return
}

func (writer *BotActionWriter) SendSwitchLane(dir Direction) (err error) {
	err = WriteMsg(writer.writer, "switchLane", dir)
	return
}

func (writer *BotActionWriter) SendTurbo() (err error) {
	err = WriteMsg(writer.writer, "turbo", "POWER!")
	return
}

func WriteMsg(writer *bufio.Writer, msgtype string, data interface{}) (err error) {
	m := map[string]interface{}{
		"msgType":  msgtype,
		"data":     data,
		"gameTick": currentTick,
	}
	payload, err := json.Marshal(m)
	_, err = writer.Write(append(payload, byte('\n')))
	if err != nil {
		return
	}
	writer.Flush()
	return
}
