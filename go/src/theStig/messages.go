package main

import (
	"bufio"
	"encoding/json"
	"net"
)

type GameMessageReader struct {
	decoder *json.Decoder
}

func CreateGameMessageReader(conn net.Conn) *GameMessageReader {
	return &GameMessageReader{
		decoder: json.NewDecoder(bufio.NewReader(conn)),
	}
}

func (reader *GameMessageReader) ReadMsg() (Message, error) {
	m := Message{}
	if err := reader.decoder.Decode(&m); err != nil {
		return m, err
	}
	return m, nil
}

type Message struct {
	MsgType  string
	Data     json.RawMessage
	GameId   string
	GameTick int
}

type Join struct {
	Name string
	Key  string
}

type GameInit struct {
	Race Race
}

type Race struct {
	Track       Track
	Cars        []*Car
	RaceSession RaceSession
}

type Track struct {
	Id     string
	Name   string
	Pieces []*Piece
	Lanes  []Lane
}

type Piece struct {
	Length float64
	Switch bool
	Radius int
	Angle  float64
}

type Lane struct {
	DistanceFromCenter int
	Index              int
}

type Car struct {
	Id         CarId
	Dimensions CarDimensions
}

type CarId struct {
	Name  string
	Color string
}

type CarDimensions struct {
	Length            float64
	Width             float64
	GuideFlagPosition float64
}

type CarPosition struct {
	Id            CarId
	Angle         float64
	PiecePosition PiecePosition
}

type PiecePosition struct {
	PieceIndex      int
	InPieceDistance float64
	Lane            CarLane
	Lap             int
}

type CarLane struct {
	StartLaneIndex int
	EndLaneIndex   int
}

type RaceSession struct {
	Laps         int
	MaxLapTimeMs int
	QuickRace    bool
}
