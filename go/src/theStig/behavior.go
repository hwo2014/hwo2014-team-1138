package main

import (
	"fmt"
	"math"
)

var (
	TrackPlan    []Direction
	lastPosition CarPosition
	mass         float64
	drag         float64
)

// Using the first 3 CarPositions, calculate the drag constant and the mass
// of the car.
func CalculateThrottle(currPosition CarPosition) float64 {
	// fmt.Printf("Tick: %d\n", currentTick)
	// fmt.Printf("Prev: %+v\n", PrevPosition)
	// fmt.Printf("Curr: %+v\n", CurrPosition)
	if currentTick < 3 {
		return 1.0
	} else if currentTick == 3 {
		v1 := lastPosition.PiecePosition.InPieceDistance
		v2 := currPosition.PiecePosition.InPieceDistance - v1
		drag = CalculateDrag(v1, v2)
		mass = CalculateCarMass(v1, v2, drag)
		fmt.Printf("Tick: %d\n", currentTick)
		fmt.Printf("Prev: %+v\n", lastPosition)
		fmt.Printf("Curr: %+v\n", currPosition)
		fmt.Printf("Drag: %+v\n", drag)
		fmt.Printf("Mass: %+v\n", mass)
	}
	return .65
}

func CalculateDrag(v1, v2 float64) float64 {
	return (v1 - (v2 - v1)) / math.Pow(v1, 2)
}

func CalculateCarMass(v1, v2, drag float64) float64 {
	return 1.0 / (math.Log((v2-(1.0/drag))/(v1-(1.0/drag))) / (-drag))
}

func CreateTrackPlan(pieces []*Piece) {
	size := len(pieces)
	TrackPlan = make([]Direction, size)
	planIndex := 0
	for _, section := range splitIntoSections(pieces) {
		sectionAngleSum := angleSum(section)
		var dir Direction
		if sectionAngleSum > 0 {
			dir = Right
		} else if sectionAngleSum < 0 {
			dir = Left
		}
		// Need to set the piece before the switch with the lane change
		// direction
		TrackPlan[(size+planIndex-2)%size] = dir
		planIndex = planIndex + len(section)
	}
}

// This will split the section into n sections between switches
func splitIntoSections(pieces []*Piece) [][]*Piece {
	sections := [][]*Piece{}
	for i := 0; i < len(pieces); i++ {
		for j, np := range pieces[i:] {
			if np.Switch {
				sections = append(sections, pieces[i:i+j+1])
				i += j
				break
			}
		}
	}
	return sections
}

// given a list of pieces, calculate the total angle sum amongst them
func angleSum(pieces []*Piece) float64 {
	sum := 0.0
	for _, p := range pieces {
		sum += p.Angle
	}
	return sum
}
