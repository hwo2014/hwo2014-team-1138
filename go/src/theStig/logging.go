package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
)

var (
	racelog *log.Logger
)

func SetRaceLogger(gameId string) {
	var logFile io.Writer
	if debug {
		logFile = OpenLogFile(gameId)
	} else {
		logFile = ioutil.Discard
	}

	racelog = log.New(logFile, "", log.Ltime)
}

func OpenLogFile(gameId string) *os.File {
	log_path := fmt.Sprintf("src/racelogs/%s.log", gameId)
	f, err := os.OpenFile(log_path, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	return f
}
